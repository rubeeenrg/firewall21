#! /bin/bash

# ==============================================
# Activar si el host ha de fer de router
#echo 1 > /proc/sys/net/ipv4/ip_forward

# Regles Flush: buidar les regles actuals
iptables -F
iptables -X
iptables -Z
iptables -t nat -F

# Establir la política per defecte (ACCEPT o DROP)
iptables -P INPUT ACCEPT
iptables -P OUTPUT ACCEPT
iptables -P FORWARD ACCEPT
iptables -t nat -P PREROUTING ACCEPT
iptables -t nat -P POSTROUTING ACCEPT

# Filtrar ports (personalitzar) - - - - - - - - - - - - - - - - - - - - - - -
# ----------------------------------------------------------------------------
# Permetre totes les pròpies connexions via localhost
iptables -A INPUT -i lo -j ACCEPT
iptables -A OUTPUT -o lo -j ACCEPT

# Permetre tot el trafic de la pròpia ip(10.200.243.224))
iptables -A INPUT -s 10.200.243.224 -j ACCEPT
iptables -A OUTPUT -d 10.200.243.224 -j ACCEPT
# Aplicar altres regles per obrir i tancar ports
# ....
# Aplicar altres regles per permetre o no tipus de trafic
# ....
# ----------------------------------------------------------------------------
# Mostrar les regles generades
#iptables -L -t nat

# 4) Evitar que es falsifiqui la ip de origen: SPOOFING (interfície 'br..' = bridge)
iptable -A FORWARD ! -s 172.19.0.0/24 -i br-7d521247ea41 -j DROP
iptable -A FORWARD ! -s 172.20.0.0/24 -i br-7d521247ea41 -j DROP

# 3) El Router ha de xapar la connexió entre A1 (Xarxa A) i A2 (Xarxa A):
# NO POT! EL ROUTER POT XAPAR LES PETICIONS QUE SORTIN DE LA XARXA A I QUE ENTRIN CAP AQUESTA PERÒ NO POT XAPAR LES PETICIONS ENTRE DISPOSITIUS
# QUE ESTIGUIN DINS DE LA MATEIXA XARXA!

# 2) Xarxa A permetre navegar per internet però res més a l'exterior (docker0 = interfície de la xarxa a | eth0 = interfície del router:
iptables -A FORWARD -s 172.19.0.0/24 -d 0.0.0.0/0 -p tcp --dport 80 -j ACCEPT
#iptables -A FORWARD -i docker0 -o eth0 -p tcp --dport 80 -j ACCEPT
iptables -A FORWARD -s 0.0.0.0/24 -p tcp --sport 80 -d 172.19.0.0/24 \
            -m state --state RELATED,ESTABLISHED -j ACCEPT
#iptables -A FORWARD -i eth0 -p tcp --sport 80 -o docker0 \
#            -m state --state RELATED,ESTABLISHED -j ACCEPT
iptables -A FORWARD -s 172.19.0.0/24 -d 0.0.0.0/0 -j REJECT 
#iptables -A FORWARD -i docker0 -o eth0 -j REJECT     
iptables -A FORWARD -s 0.0.0.0/0 -o 172.19.0.0/24 -j REJECT
#iptables -A FORWARD -i eth0 -o docker0 -j REJECT

# 1) La Xarxa A (172.19.0.0/24 | docker0) no pot communicar-se amb la Xarxa B (172.20.0.0/24 | docker1):
# docker[0/1] --> nom de la interfície
iptables -A FORWARD -s 172.19.0.0/24 -d 172.20.0.0/4 -j DROP
#iptables -A FORWARD -i docker0 -o docker1 -j DROP
#iptables -A FORWARD -i docker1 -o docker0 -m state --state RELATED,ESTABLISHED -j DROP #--> Depén de quin protocl parlem (icmp, tcp, upd...)
