#! /bin/bash
# @edt ASIX M11-SAD Curs 2018-2019
# iptables

#echo 1 > /proc/sys/ipv4/ip_forward

# Regles flush
iptables -F
iptables -X
iptables -Z
iptables -t nat -F

# Polítiques per defecte: 
iptables -P INPUT ACCEPT
iptables -P OUTPUT ACCEPT
iptables -P FORWARD ACCEPT
iptables -t nat -P PREROUTING ACCEPT
iptables -t nat -P POSTROUTING ACCEPT

# obrir el localhost
iptables -A INPUT  -i lo -j ACCEPT
iptables -A OUTPUT -o lo -j ACCEPT

# obrir la nostra ip
iptables -A INPUT -s 192.168.10.1 -j ACCEPT
iptables -A OUTPUT -d 192.168.10.1 -j ACCEPT

iptables -A INPUT -s 192.168.11.1 -j ACCEPT
iptables -A OUTPUT -d 192.168.11.1 -j ACCEPT

# 2) Fer NAT per les xarxes internes:
# - 192.168.10.0/24
# - 192.168.11.0/24
iptables -t nat -A POSTROUTING -s 192.168.10.0/24 -o eno1 -j MASQUERADE
iptables -t nat -A POSTROUTING -s 192.168.11.0/24 -o eno1 -j MASQUERADE

# 3) Regles FORWARD, implementar una o més regles FORWARD
# A1 NO POT ACCEDIR A LA XARXA B | A1 --> NO POT FER PING A B1 i B2 | A1 --> POT FER PING A ROUTER I A 1.1.1.1
#iptables -A FORWARD -d 192.168.11.0/24 -p tcp --sport 80 -i eno1 -m state --state RELATED,ESTABLISHED -j ACCEPT
#iptables -A FORWARD -s 192.168.10.2/32 -d 192.168.11.0/24 -j REJECT

# 4) IMPLEMENTAR UNA O MÉS REGLES PORT FORWARDING (PORT 5001 AL SSH D'A1 | PORT 5002 AL SSH D'A2)
#iptables -t nat -A PREROUTING -p tcp --dport 5001 -j DNAT \
#          --to 192.168.10.2:80
#iptables -t nat -A PREROUTING -p tcp --dport 5002 -j DNAT \
#          --to 192.168.11.3:80

# EXERCICI 5 I 6 (VAN JUNTS JA QUE FEM ACCEPT EN EL 5 I NECESSITEM FER DROP):
# 5) ELS HOSTS 'XARXA A' I 'XARXA B' NO PODEN FER PING A L'EXTERIOR I SI A L'INTERIOR
iptables -A FORWARD -p icmp --icmp-type 8 -s 192.168.10.0/24 -d 192.168.11.0/24 -j ACCEPT
# 6) EL ROUTER NO CONTESTA PINGS PERÒ SI EN POT FER
iptables -A FORWARD -p icmp --icmp-type 8 -s 192.168.10.0/24 -d 0.0.0.0/0 -j DROP
iptables -A FORWARD -p icmp --icmp-type 8 -s 192.168.11.0/24 -d 0.0.0.0/0 -j DROP
