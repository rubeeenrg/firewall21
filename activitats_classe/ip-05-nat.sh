#! /bin/bash

# ==============================================
# Activar si el host ha de fer de router
echo 1 > /proc/sys/net/ipv4/ip_forward

# Regles Flush: buidar les regles actuals
iptables -F
iptables -X
iptables -Z
iptables -t nat -F

# Establir la política per defecte (ACCEPT o DROP)
iptables -P INPUT ACCEPT
iptables -P OUTPUT ACCEPT
iptables -P FORWARD ACCEPT
iptables -t nat -P PREROUTING ACCEPT
iptables -t nat -P POSTROUTING ACCEPT

# Filtrar ports (personalitzar) - - - - - - - - - - - - - - - - - - - - - - -
# ----------------------------------------------------------------------------
# Permetre totes les pròpies connexions via localhost
iptables -A INPUT -i lo -j ACCEPT
iptables -A OUTPUT -o lo -j ACCEPT

# Permetre tot el trafic de la pròpia ip(10.200.243.224))
iptables -A INPUT -s 10.200.243.224 -j ACCEPT
iptables -A OUTPUT -d 10.200.243.224 -j ACCEPT

# Aplicar altres regles per obrir i tancar ports
# ....
# Aplicar altres regles per permetre o no tipus de trafic
# ....
# ----------------------------------------------------------------------------
# Mostrar les regles generades
#iptables -L -t nat

# 5) Obrir el port 5001 del router i aquest faci 'FORWARDING' cap al port 13 de la màquina A1 (NetA):
# Abans de que passi per el router, canviarà el destí | DNAT --> Destination NAT --> Canviar el destí!
iptables -t nat -A PREROUTING -i eno1 -p tcp --dport 5001 -j DNAT \
	 --to 172.21.0.2:13
iptables -A FORWARD -d 172.21.0.2 -p tcp --dport 13 -j DROP
# Assegurar-se que regles 'forward' permeten aquest tràfic

# 4) NetA només pot navegar cap a internet:
# Habitem tràfic per DNS
#iptables -A FORWARD -s 172.21.0.0/24 -p udp -o eno1 --dport 53 -j ACCEPT
#iptables -A FORWARD -i eno1 -p udp -d 172.21.0.0/24 --sport 53 -j ACCEPT

#iptables -A FORWARD -s 172.21.0.0/24 -o eno1 -p tcp --dport 80 -j ACCEPT
# --sport --> que provingui del port 80!
#iptables -A FORWARD -d 172.21.0.0/24 -p tcp --sport 80 -i eno1 \
#         -m state --state RELATED,ESTABLISHED -j ACCEPT
#iptables -A FORWARD -s 172.21.0.0/24 -o eno1 -j REJECT 
#iptables -A FORWARD -d 172.21.0.0/24 -i eno1 -j REJECT

# 3) A1 (NetA) no pot navegar per internet (Ni cap a fora ni que retorni):
#iptables -A FORWARD -s 172.21.0.2 -p tcp -o eno1 --dport 80 -j REJECT
#iptables -A FORWARD -i eno1 -p tcp --sport 80 -d 172.21.0.2 -j REJECT

# 2) A1 (NetA) no pot accedir a B1 (NetB):
#iptables -A FORWARD -s 172.21.0.2 -d 172.22.0.2 -j DROP

# 1) Fer NAT per les xarxes internes:
#	- 172.21.0.0/24
#	- 172.22.0.0/24
# MASQUERADE (Enganyar)
# Tot el que s'emmascara de sortida quan torna ja està desenmascarat, per tant, no cal dir-li res de sortida
# POSTROUTING --> Quan el paquet arribi al router, aquest decidirà per quina interfície enviar-ho, abans d'enviar-ho, canvia la IP!
iptables -t nat -A POSTROUTING -s 172.21.0.0/24 -o eno1 -j MASQUERADE
iptables -t nat -A POSTROUTING -s 172.22.0.0/24 -o eno1 -j MASQUERADE
